DROP TABLE if exists Revue;
DROP TABLE if exists Auteur;
DROP TABLE if exists Numero;
DROP TABLE if exists Article;
DROP TABLE if exists FaitReference;
DROP TABLE if exists Ecrit;

CREATE TYPE periodique AS ENUM ('m','t','s','a');
CREATE TYPE typeRevue AS ENUM ('scientifique','magazine');
CREATE TYPE typeAuteur AS ENUM ('chercheur','journaliste');

CREATE TABLE Revues (
	nom VARCHAR(256) PRIMARY KEY,
	periodicite periodique NOT NULL,
	type typeRevue NOT NULL,
	specialite VARCHAR(256),
	domaineRecherche VARCHAR(256),
	CHECK ( (type='scientifique' AND specialite IS NOT NULL) AND (type='magazine' AND domaineRecherche IS NOT NULL) )
);

CREATE TABLE Auteurs (
	idAuteur INTEGER PRIMARY KEY,
	nom VARCHAR(256) NOT NULL,
	prenom VARCHAR(256) NOT NULL,
	email VARCHAR(256) NOT NULL,
	type typeAuteur NOT NULL,
	nbAnneesExperience INTEGER,
	CHECK (type='chercheur' AND nbAnneesExperience IS NOT NULL)	
);

CREATE TABLE Numero (
	revue VARCHAR(256) REFERENCES Revues(nom),
	num INTEGER,
	annee INTEGER NOT NULL,
	nbPages INTEGER NOT NULL,
	CHECK (annee >0),
	CHECK (nbPages >0),
	PRIMARY KEY (revue,num)
);

CREATE TABLE Articles(
	titre VARCHAR(256) PRIMARY KEY,
	contenue VARCHAR(256),
	revue VARCHAR(256) REFERENCES Numero(revue) NOT NULL,
	numero VARCHAR(256) REFERENCES Numero(num) NOT NULL 	
);

CREATE TABLE FaitReference (
	referant VARCHAR(256) REFERENCES Articles(titre),
	referee VARCHAR(256) REFERENCES Articles(titre),
	PRIMARY KEY (referant,referee)
);

CREATE TABLE Ecrit(
	auteur INTEGER REFERENCES Auteurs(idAuteur),
	article VARCHAR(256) REFERENCES Articles(titre),
	PRIMARY KEY (auteur,article)
);
	
	
